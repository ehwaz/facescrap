$(document).ready(function(){
    var c = 1;
    $(".timeline_post").draggable({
        appendTo: "body",
        helper: "clone",
    });
    $("#scrapbook").droppable({
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        accept: ".timeline_post",
        drop: function(event, ui){
            var temp_scrap = ui.draggable.clone().attr("id", "temp_scrap_" + c);
            $(temp_scrap).removeClass("timeline_post ui-draggable");
            $(temp_scrap).addClass("scrap_temp_post");
            $(this).find("#scrap_list").prepend(temp_scrap);
            $("#tag_null").clone().attr("id", "tag_"+c).appendTo($('#temp_scrap_'+ c).find('.fb_post'));
            $("#tag_"+ c++).show();
            $( this ).find( '.scrap_cancel_button' ).show();
        }
    });

    var loading= false;
    $('#timeline').scroll(function() {
        if ( $(this).outerHeight() + $(this).scrollTop() > $(this)[0].scrollHeight) {
            loading= true;
            // your content loading call goes here.
            //TODO: AJAX call to load more posts & Display them
            $('#hidden_post').show();
            loading = false; // reset value of loading once content loaded
        }
    });

    $("#scrapbook").on("click", ".scrap_cancel_button", function(evt){
        evt.preventDefault();
        $(this).closest("li").remove();
    });

    $(".scrap_done_button").click(function(){

    });
});