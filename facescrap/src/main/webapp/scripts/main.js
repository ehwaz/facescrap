$(document).ready(function () {
    var divTags = $('<div id="user_tags"></div>');
    divTags.appendTo($("div#user_info"));

    $(document).on("tagchange", function () {
        $.ajax("/tags").done(function (data) {
            divTags.empty();

            if (!data || !data.length) return;
            $.each(data, function (i, item) {
                var lnkTag = $('<a></a>');
                lnkTag.text(item.name);
                lnkTag.attr("href", "/scrapbook/?tags=" + item.tagId);

                lnkTag.attr("data-id", item.tagId);
                lnkTag.attr("data-name", item.name);

                divTags.append(lnkTag);
            });

            $(document).trigger("tagchangedone", [data]);
        });
    });

    $(document).trigger("tagchange");
    $(document).on("click", ".see_more_button", function (evt) {
        evt.preventDefault();

        var hidden_comments = $(this).closest(".comment_area").find(".hidden_comment");
        hidden_comments = hidden_comments.slice(0, hidden_comments.length - 1);
        for (var i = 0; i < 5; i++) {
            if (i >= hidden_comments.length) {
                $(this).parent().hide();
                break;
            }

            $(hidden_comments[i]).show();
            $(hidden_comments[i]).removeClass("hidden_comment");
        }
    });

});