var treeHeightOffset = 20;
var treeHeightGap = 90;
var treeWidth = 580;
var minNodeWidth = 40;
var draggingObject = null;
var temp_tag_tree;
var temp_tag_tree_canvas;
var node_fill_color = 'rgb(53,57,66)';
var tree_line_color = 'rgb(84,96,110)';

$(document).ready(function () {
    /* TODO: Stub tree를 DB에서 갖고온 트리로 바꾸기*/
    var stub_tree1 = [
        {id: 1,
            name: "Food",
            children: [
                {id: 2, name: "Fast Food",
                    children: [
                        {id: 3, name: "Burger King", children: [
                            {id: 10, name: "Waffer Junior", children: []},
                            {id: 11, name: "Crispy Chicken", children: []},
                            {id: 12, name: "Garlic Steak", children: []},
                        ]},
                        {id: 4, name: "Lotte Ria", children: []}
                    ]},
                {id: 5, name: "Korean Food",
                    children: [
                        {id: 6, name: "Kimchi", children: []},
                        {id: 7, name: "Rice cake", children: []},
                        {id: 8, name: "DDukBBoKi", children: []}
                    ]}
            ]
        }
    ];

    var stub_tree2 = [
        {id: 21,
            name: "미남미녀",
            children: [
                {id: 2, name: "아이돌",
                    children: [
                        {id: 3, name: "손나은", children: []},
                        {id: 4, name: "수지", children: []},
                        {id: 99, name: "아이유", children: []},
                        {id: 99, name: "설리", children: []}
                    ]},
                {id: 5, name: "외쿡미녀",
                    children: [
                        {id: 6, name: "Chloe Moretz", children: []},
                        {id: 7, name: "아만다 사이프리드", children: []},
                        {id: 8, name: "나탈리포트만", children: []}
                    ]}
            ]
        }
    ];

    var trees = [stub_tree1, stub_tree2];

    $.ajax("/trees").done(function (data) {
        trees = data;

        for (var i = 0; i < trees.length; i++) {
            trees[i] = [trees[i]];
        }

        for (var i = 0; i < trees.length; i++) {
            var tree_container = $('#tag_tree_null').clone().attr("id", "tag_tree_container_" + i);
            tree_container.attr('tree_id', i);
            var tree_div = tree_container.find('.tag_tree_div');
            tree_div.attr('id', "tag_tree_" + i);
            $('#tagtree_content').append(tree_container);
            tree_container.show();
            draw_big_tree(trees[i], 0, 10, "tag_tree_" + i, false);
        }
    });

    $(document).on("tagchangedone", function () {
        $("#user_tags > a").draggable({
            appendTo: "body",
            helper: function (event) {
                draggingObject = $(this).clone();
                return draggingObject;
            }
        });
    });

    /** Button Actions */
    var add_tree = false;
    $('#add_new_tree').click(function () {
        if (add_tree) {
            alert("지금 만들고 있는 거 다 만들고 새로 만드셈!");
            return;
        }
        temp_tag_tree = [
            {id: -1, name: "", children: []}
        ];
        make_tree_edit_canvas(temp_tag_tree);
        add_tree = true;
    });

    $("#tag_tree_book").on("click", ".done_button", function (evt) {
        evt.preventDefault();
        add_tree = false;
        if (confirm("이거 저장할 거임?")) {
            $.post("/tree", {tree: JSON.stringify(temp_tag_tree[0])}).done(function () {
                location.reload();
            });
        }
    });

    $("#tag_tree_book").on("click", ".edit_button", function (evt) {
        evt.preventDefault();
        if (add_tree) {
            alert("지금 만들고 있는 거 다 만들고 고치셈!");
            return;
        }
        temp_tag_tree = trees[$(this).parent().parent().attr('tree_id')];
        make_tree_edit_canvas(temp_tag_tree);
        add_tree = true;
    });

    $("#tag_tree_book").on("click", ".delete_button", function (evt) {
        evt.preventDefault();
        var tree_container = $(this).parent().parent();
        var message = "이거 트리 통째로 지울거임?";
        var is_temp = false;
        if (tree_container.attr('id') == "tag_tree_container_temp") {
            is_temp = true;
            message = "지금 만들던/고치던 트리 다 취소함?";
        }
        if (confirm(message)) {
            if (!is_temp) {
                var treeId = trees[tree_container.attr("tree_id")][0].tagTreeId;

                /* TODO: DB에서 트리 지우기*/
                $.post("/trees/delete", {id: treeId}).done(function () {
                    tree_container.remove();
                    add_tree = false;
                });
            } else {
                tree_container.remove();
                add_tree = false;
            }
        }
    });
});

function make_tree_edit_canvas(temp_tag_tree) {
    var tree_container = $('#tag_tree_null').clone().attr("id", "tag_tree_container_temp");
    var tree_div = tree_container.find('.tag_tree_div');
    tree_div.attr('id', "tag_tree_temp");
    $('#tagtree_content_top').after(tree_container);
    tree_container.show();
    tree_container.find('.edit_button').hide();
    tree_container.find('.done_button').show();
    temp_tag_tree_canvas = draw_big_tree(temp_tag_tree, 0, 10, "tag_tree_temp", true);
}

function add_node(tree, parent, node) {
    if (parent == null) {
        tree = [node];
    } else {
        parent.children.push(node);
    }
    return tree;
}

function find_node(tree, id) {
    if (tree.length == 0)
        return null;

    for (var i = 0; i < tree.length; i++) {
        if (tree[i].id == id)
            return tree[i];
        else
            node = find_node(tree[i].children, id);

        if (node != null)
            return node;
    }

    return null;
}

function find_parent_node(tree, node) {
    var parent_node = null;
    if (tree.length == 0)
        return null;
    for (var i = 0; i < tree.length; i++) {
        if ($.inArray(node, tree[i].children) >= 0)
            return tree[i];
    }
    for (var i = 0; i < tree.length; i++) {
        parent_node = find_parent_node(tree[i].children, node);
        if (parent_node != null)
            return parent_node;
    }
    return parent_node;
}

function del_node(tree, id) {
    var target_node = find_node(tree, id);
    var parent_node = find_parent_node(tree, target_node);
    if (parent_node == null)
        tree = [
            {id: -1, name: "", children: []}
        ];
    else
        parent_node.children.splice($.inArray(target_node, parent_node.children), 1);
    return tree;
}

function handle_drop(rect) {
    if (draggingObject) {
        var parent = find_node(temp_tag_tree, rect.getId());
        var node = {id: draggingObject.attr("data-id"), name: draggingObject.attr("data-name"), children: []};
        if (find_node(temp_tag_tree, node.id)) {
            alert("이미 있는 거임 추가 ㄴㄴ해");
            draggingObject = null;
            return;
        }
        temp_tag_tree = add_node(temp_tag_tree, parent, node);
        temp_tag_tree_canvas.remove();
        temp_tag_tree_canvas = draw_big_tree(temp_tag_tree, 0, 10, "tag_tree_temp", true);
        draggingObject = null;
    }
}

function handle_del(button) {
    if (confirm("참고로 자식까지 다 지워짐 ㅇㅇ?")) {
        temp_tag_tree = del_node(temp_tag_tree, button.getId());
        temp_tag_tree_canvas.remove();
        temp_tag_tree_canvas = draw_big_tree(temp_tag_tree, 0, 10, "tag_tree_temp", true);
    }
}

function draw_empty_node(node, depth, offsetX, nodeWidth, layer) {
    var nodeX = offsetX;
    var nodeY = treeHeightOffset + treeHeightGap * depth;
    var rect = new Kinetic.Rect({
        x: nodeX,
        y: nodeY,
        stroke: '#eee',
        strokeWidth: 3,
        dashArray: [10, 7],
        width: nodeWidth,
        height: 40,
        cornerRadius: 10,
        name: 'empty_node',
    });

    if (depth > 0) {
        var line = new Kinetic.Line({
            points: [
                [nodeX + nodeWidth / 2, nodeY - treeHeightGap / 4],
                [nodeX + nodeWidth / 2, nodeY]
            ],
            stroke: '#555',
            strokeWidth: 3,
            lineCap: 'round',
            lineJoin: 'round'
        });

        layer.add(line);
    }

    rect.on('mouseup', function (evt) {
        handle_drop(evt.targetNode);
    });

    layer.add(rect);

    return [
        [nodeX + nodeWidth / 2, nodeY - treeHeightGap / 4],
        [nodeX + nodeWidth / 2, nodeY + rect.getHeight()],
        rect.getHeight()
    ];
}

function draw_node(node, depth, offsetX, nodeWidth, layer, edit_flag) {
    var nodeX = offsetX;
    var nodeY = treeHeightOffset + treeHeightGap * depth;
    var text = new Kinetic.Text({
        x: nodeX,
        y: nodeY,
        text: node.name,
        fontSize: 15 - depth,
        fontFamily: 'Verdana',
        fill: 'white',
        width: nodeWidth,
        padding: 10,
        align: 'center',
        id: node.id,
    });

    var rect = new Kinetic.Rect({
        x: nodeX,
        y: nodeY,
        stroke: tree_line_color,
        strokeWidth: 3,
        fill: node_fill_color,
        width: nodeWidth,
        height: text.getHeight(),
        cornerRadius: 10,
        id: node.id,
    });

    if (depth > 0) {
        var line = new Kinetic.Line({
            points: [
                [nodeX + nodeWidth / 2, nodeY - treeHeightGap / 4],
                [nodeX + nodeWidth / 2, nodeY]
            ],
            stroke: tree_line_color,
            strokeWidth: 3,
            lineCap: 'round',
            lineJoin: 'round',
        });

        layer.add(line);
    }


    layer.add(rect);
    layer.add(text);


    if (edit_flag) {
        rect.setFill('white');
        text.setFill(tree_line_color);

        text.on('mouseup', function (evt) {
            handle_drop(evt.targetNode);
        });
        text.on('mouseover', function (evt) {
            rect.setFill('#fbb');
            layer.draw();
        });
        text.on('mouseout', function (evt) {
            rect.setFill('white');
            layer.draw();
        });

        rect.on('mouseup', function (evt) {
            handle_drop(evt.targetNode);
        });
        rect.on('mouseover', function (evt) {
            evt.targetNode.setFill('#fbb');
            layer.draw();
        });
        rect.on('mouseout', function (evt) {
            evt.targetNode.setFill('white');
            layer.draw();
        });

        var del_button = new Kinetic.Text({
            x: nodeX,
            y: nodeY - 21,
            text: "x",
            fontSize: 15,
            fontFamily: 'Comic Sans MS',
            fill: '#ddd',
            width: 20,
            id: node.id,
        });

        del_button.on('click', function (evt) {
            handle_del(evt.targetNode);
        });

        layer.add(del_button);
    }

    return [
        [nodeX + nodeWidth / 2, nodeY - treeHeightGap / 4],
        [nodeX + nodeWidth / 2, nodeY + rect.getHeight()],
        rect.getHeight()
    ];
}

function draw_tree(tree, depth, offsetX, width, stage, edit_flag) {
    var layer = new Kinetic.Layer();
    var nodeWidth = 80;
    var subtreeWidth = width / tree.length;
    var nodePrevTopCoord = null;
    var success = true;
    if (tree.length != 0) {
        nodeWidth = Math.min(nodeWidth, subtreeWidth - 20);
        if (nodeWidth < minNodeWidth) {
            return false;
        }
        for (var i = 0; i < tree.length; i++) {
            if (tree[i].id == -1) {
                nodeCoords = draw_empty_node(tree[i], depth, offsetX + (subtreeWidth - nodeWidth) / 2, nodeWidth, layer);
            } else {
                nodeCoords = draw_node(tree[i], depth, offsetX + (subtreeWidth - nodeWidth) / 2, nodeWidth, layer, edit_flag);
            }
            if (nodePrevTopCoord != null) {
                var line_h = new Kinetic.Line({
                    points: [nodePrevTopCoord, nodeCoords[0]],
                    stroke: tree_line_color,
                    strokeWidth: 3,
                    lineCap: 'round',
                    lineJoin: 'round',
                });
                layer.add(line_h);
            }
            nodePrevTopCoord = nodeCoords[0];
            nodeBottomCoord = nodeCoords[1];
            var treeIter = tree[i].children;
            if (treeIter.length != 0) {
                nodeHeight = nodeCoords[2];
                var line_v = new Kinetic.Line({
                    points: [nodeBottomCoord, [nodeBottomCoord[0], nodeBottomCoord[1] + (3 * treeHeightGap / 4 - nodeHeight)]],
                    stroke: tree_line_color,
                    strokeWidth: 3,
                    lineCap: 'round',
                    lineJoin: 'round',
                });
                layer.add(line_v);
            }
            success = draw_tree(treeIter, depth + 1, offsetX, subtreeWidth, stage, edit_flag);
            if (!success)
                return success;
            offsetX += subtreeWidth;
        }
    }
    stage.add(layer);
    return true;

}

function draw_big_tree(tree, depth, offsetX, container_name, edit_flag) {
    var tree_width = treeWidth;
    var stage = new Kinetic.Stage({
        container: container_name,
        width: tree_width + 20,
        height: 400,
    });

    while (!draw_tree(tree, depth, offsetX, tree_width, stage, edit_flag)) {
        stage.remove();
        tree_width += 100;
        stage = new Kinetic.Stage({
            container: container_name,
            width: tree_width + 20,
            height: 400,
        });
    }
    return stage;
}
