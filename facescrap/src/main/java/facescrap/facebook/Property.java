package facescrap.facebook;

/**
 * Created with IntelliJ IDEA.
 * User: gihopark
 * Date: 2013. 12. 2.
 * Time: 오후 8:50
 * To change this template use File | Settings | File Templates.
 */
public class Property {
    private String name;
    private String text;
    private String href;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
