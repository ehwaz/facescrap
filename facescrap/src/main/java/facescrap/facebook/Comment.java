package facescrap.facebook;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: gihopark
 * Date: 2013. 12. 2.
 * Time: 오후 8:54
 * To change this template use File | Settings | File Templates.
 */
public class Comment {
    private String id;
    private Reference from;
    private String message;
    private Date created_time;
    private String like_count;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Reference getFrom() {
        return from;
    }

    public void setFrom(Reference from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Date created_time) {
        this.created_time = created_time;
    }

    public String getLike_count() {
        return like_count;
    }

    public void setLike_count(String like_count) {
        this.like_count = like_count;
    }
}
