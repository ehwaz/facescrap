package facescrap.facebook;

/**
 * Created with IntelliJ IDEA.
 * User: gihopark
 * Date: 2013. 12. 2.
 * Time: 오후 8:42
 * To change this template use File | Settings | File Templates.
 */
public class Reference {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
