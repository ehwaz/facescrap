package facescrap.util;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import kr.ac.kaist.swrc.jhannanum.comm.Eojeol;
import kr.ac.kaist.swrc.jhannanum.comm.Sentence;
import kr.ac.kaist.swrc.jhannanum.hannanum.Workflow;
import kr.ac.kaist.swrc.jhannanum.plugin.MajorPlugin.MorphAnalyzer.ChartMorphAnalyzer.ChartMorphAnalyzer;
import kr.ac.kaist.swrc.jhannanum.plugin.MajorPlugin.PosTagger.HmmPosTagger.HMMTagger;
import kr.ac.kaist.swrc.jhannanum.plugin.SupplementPlugin.MorphemeProcessor.UnknownMorphProcessor.UnknownProcessor;
import kr.ac.kaist.swrc.jhannanum.plugin.SupplementPlugin.PlainTextProcessor.InformalSentenceFilter.InformalSentenceFilter;
import kr.ac.kaist.swrc.jhannanum.plugin.SupplementPlugin.PlainTextProcessor.SentenceSegmentor.SentenceSegmentor;
import kr.ac.kaist.swrc.jhannanum.plugin.SupplementPlugin.PosProcessor.NounExtractor.NounExtractor;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import facescrap.model.Article;
import facescrap.model.TagArticle;
import facescrap.util.Similarity;

// post 내용을 입력받아 추천 태그를 반환하는 클래스. 최초 생성후 재활용이 가능하다.
@Component
public class TagGenerator {
	Workflow workflow;
	@Autowired
    private Similarity similarity;
	
	@Autowired
    private SessionFactory sessionFactory;
	
	public TagGenerator () {
		// Sangwook: 매우 중요!!! 형태소 분석기에서 사용하는 data, conf 경로를 Workflow 클래스 생성자의 인자값으로 주어야 한다.
		//			data, conf는 프로젝트 폴더에 포함되어 있으므로, 아래와 같이 myLocalRepoPath를 수정해줄 것.
		String myLocalRepoPath = "-여기에-경로를-넣어요-";
		String tagGenResourcePath = "/FaceScrap/facescrap";
		workflow = new Workflow(myLocalRepoPath + tagGenResourcePath);
		workflow.appendPlainTextProcessor(new SentenceSegmentor(), null);
		workflow.appendPlainTextProcessor(new InformalSentenceFilter(), null);
			
		workflow.setMorphAnalyzer(new ChartMorphAnalyzer(), "conf/plugin/MajorPlugin/MorphAnalyzer/ChartMorphAnalyzer.json");
		//workflow.appendMorphemeProcessor(new UnknownProcessor(), null);
				
		workflow.setPosTagger(new HMMTagger(), "conf/plugin/MajorPlugin/PosTagger/HmmPosTagger.json");
		workflow.appendPosProcessor(new NounExtractor(), null);

		try {
			/* Activate the work flow in the thread mode */
			/* Once a work flow is activated, it can be used repeatedly. */
			workflow.activateWorkflow(true);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	// 소멸자 대신..
	public void finalize() {
		workflow.close();
	}
	
	// 추천 태그를 얻기 위해 외부에서 사용될 유일한 메소드.
	// 입력받은 텍스트로 추천할 태그의 vector를 리턴.
	public HashMap<String, Integer> recommand(String fb_post_text, String writerOfPost, int numOfResult, int numOfSample) {
		HashMap<String, Integer> recommandation = new HashMap<String, Integer>();
		int count = 0;
		
		////-- <명사, 빈도수> 추출. (1차, 2차 처리)
		HashMap<String, Integer> extractedNouns = this.extractNouns(fb_post_text);
		HashMap<String, Integer> extractedNouns_backup = new HashMap<String, Integer>(extractedNouns);
		
		/* 3-1차 처리: 현재 <명사, 빈도수>와 db에 저장된 article들의 
		 * <명사, 빈도수>로 similarity 계산하여 top N article 추천 */
		
		// DB로부터 <article ID, 태그, 빈도수> 쿼리
		Criteria criteria = getCurrentSession().createCriteria(Article.class);
		criteria.setMaxResults(100);	// Just for now..
		List<Article> articles = criteria.list();
		
        // 가장 값이 높은 top N article 선정, 그 인덱스를 리턴
        List<Integer> topNArticleIndex = similarity.getSimilarArticles(extractedNouns, writerOfPost, articles, numOfSample);
        
        if (topNArticleIndex == null) {
        	/* 안한다. 
        	// Similar Article이 없으면 <명사, 빈도수> 추출만 리턴.
        	Iterator<String> iter = extractedNouns_backup.keySet().iterator();
    		while (iter.hasNext() && count < numOfResult) {
    			String tag = (String) iter.next();
    			// 1개짜리 단어가 너무 이상하게 짤리는 경우가 있어서.. 방지용.. 흐흑...
    			if (tag.length() > 1) {
    				recommandation.put(tag, extractedNouns_backup.get(tag));
    			}
    			count++;
    		}
    		
    		return recommandation;
    		*/
        	return null;
        } else {
        	// Similar Article이 으면 추천 판별하기 전에 아래 두가지를 수행한다.
        	// 1. 추출된 tag에서 빈도수가 2 이상일 경우 추천 결과에 무조건 넣는다.
        	// 2. 추출된 tag에서 빈도수가 1일 경우, 빈도수를 weightPlus만큼 증가시킨다. (현재 n=1)
        	//	즉, top N article의 본문에서 1번 등장한 단어보다, extractedNouns에서 1번 등장한 단어의 중요도를 더 높인다.
        	Iterator<String> iter = extractedNouns_backup.keySet().iterator();
        	int weightPlus = 1;
        	// 빈도수 추천은 3개 이하로 제한한다. (추천 시연을 위해..)
    		while (iter.hasNext() && count < 4) {
    			String tag = (String) iter.next();
    			int freq = extractedNouns_backup.get(tag);
    			if (freq > 2 && tag.length() > 1) {
    				recommandation.put(tag, freq);
    				count++;
    			}
    			else {
    				extractedNouns_backup.put(tag, freq+weightPlus);
    			}
    		}
        }
        
        /* 3-2차 처리: top N article과 extractedNouns로부터 공통 출현횟수가 가장 큰 tag word 추출 */
        HashMap<String, Integer> freqPreservingMap = new HashMap<String, Integer>(extractedNouns_backup);	// 해당 tag 자체의 freq 보존용. 메모리야 미안해..
        
        /* 3-2-1차 처리: top N article과 extractedNouns의 tag를 extractedNouns_backup으로 쓸어담는다.
         * 				이때 같은 tag가 여러번 나타나면 freq를 모두 더해서 넣는다. */
		Iterator<Integer> indexIter = topNArticleIndex.iterator();
        while (indexIter.hasNext()) {        	
            int index = (int) indexIter.next();
            Article article = articles.get(index);
            
            Criteria criteria2 = getCurrentSession().createCriteria(TagArticle.class);
            criteria2 = criteria2.add(Restrictions.eq("pk.article", article));
        	List<TagArticle> tagsOfArticle = criteria2.list();
        	
            for (int i=0; i<tagsOfArticle.size(); i++) {
              	TagArticle aTag = tagsOfArticle.get(i);
              	String tagName = aTag.getTag().getName();
              	int thisFreq = (int)aTag.getFrequency();
              	
              	if (extractedNouns_backup.containsKey(tagName)) {
              		int prevFreq = extractedNouns_backup.get(tagName);
              		extractedNouns_backup.put(tagName, prevFreq + thisFreq);
              	}
              	else {
              		freqPreservingMap.put(tagName, thisFreq);
              		extractedNouns_backup.put(tagName, thisFreq);
              	}
            }
        }
        
        // 3-2-2차 처리: TreeMap ValueComparator를 적용하여 extractedNouns_backup을 sortedTagFreqMap으로 옮긴다. 
        //				이때 각 (tag, freq)들이 freq 기준으로 정렬되어 sortedTagFreqMap에 들어간다. 메모리야 미안해..(2)
        ValueComparator valCom = new ValueComparator(extractedNouns_backup);
        TreeMap<String, Integer> sortedTagFreqMap = new TreeMap<String, Integer>(valCom);
        sortedTagFreqMap.putAll(extractedNouns_backup);
        
        // 3-2-3차 처리:
        // 축적된 freq가 높을수록(즉, extractedNouns와 top N article에 자주 등장할수록) 추천 순위가 높다.
        // 일단 freq가 낮다고 해서 특별한 제한을 두고 배제하지는 않았으며, 
        // 결과 단어 개수(numOfResult) 순위 안에 포함되는 단어는 결과에 추가된다.
        Iterator<String> iterForRecommand = sortedTagFreqMap.keySet().iterator();
		while (iterForRecommand.hasNext() && count < numOfResult) {
			String key = iterForRecommand.next();
			if (key.length() > 1) {
				recommandation.put(key, freqPreservingMap.get(key));
				count++;
			}
		}
        
        /* TODO: 3-3차 처리: User의 global tag entry와 비교?? 마마안약에에ㅔ 가능하다면... */
		
        return recommandation;		
	}
	
	// 기타 단어를 제거한뒤 품사에 따라 무의미한 명사를 제거하는 메소드 (2차 처리)
	public LinkedList<Sentence> filter(LinkedList<Sentence> sentenceList) {
		// TODO: 나중에 이 filtering을 포함하여 NounExtractor를 custom plug-in으로 수정해서 써도 될듯.
		for (Sentence s : sentenceList) {
			Eojeol[] eojeolArray = s.getEojeols();
			for (int i = 0; i < eojeolArray.length; i++) {
				if (eojeolArray[i].length > 0) {
					String[] tags = eojeolArray[i].getTags();
					for (int j = 0; j < eojeolArray[i].length; j++) {
						String tag = tags[j];
						/*
						 * 명사 종류에 대해서는 한나눔 메뉴얼 17페이지를 참조할 것.
						 * 아예 필요없을 것 같은 명사:
						 * 		지시대명사(npd): 무엇, 아무것...
						 * 		비단위성 의존 명사(nbs, nbn): 즈음, 짓...
						 * 		단위성 의존 명사(nbu): 다발, 다...
						 */
						if ( (tag.equals("npd")) || (tag.equals("nbs")) || (tag.equals("nbn")) || (tag.equals("nbu")) ) {
							eojeolArray[i].setMorpheme(j, "-");
							//eojeolArray[i].setTag(j, "-");
						}
					}
				}
			}
		}

		return sentenceList;
	}
	
	// 
	public HashMap<String, Integer> extractNouns(String document) {
		try {
			// 1차 처리: 텍스트에서 명사만 추출
			workflow.analyze(document);
			LinkedList<Sentence> resultList = workflow.getResultOfDocument(new Sentence(0, 0, false));
			
			// 2차 처리: 명사 종류를 기준으로 무의미한 명사 제거
			resultList = filter(resultList);
			
			// 빈도수 검사하여 리턴 Map 생성
			HashMap<String, Integer> tagMap = new HashMap<String, Integer>();
			for (Sentence s : resultList) {
				Eojeol[] eojeolArray = s.getEojeols();
				for (int i = 0; i < eojeolArray.length; i++) {
					if (eojeolArray[i].length > 0) {
						String[] morphemes = eojeolArray[i].getMorphemes();
						for (int j = 0; j < morphemes.length; j++) {
							String word = morphemes[j];
							// 2차 처리에서 제거되었으면 리턴 Map에 안넣는다.
							if (word.equals("-") == false) {
								if (tagMap.containsKey(word)) {
									tagMap.put(word, tagMap.get(word)+1);
								}
								else {
									tagMap.put(word, 1);
								}
							}
						}
					}
				}
			}
			return tagMap;

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		return null; 	
	}
	
	protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
}

class ValueComparator implements Comparator<String> {

    HashMap<String, Integer> base;
    public ValueComparator(HashMap<String, Integer> base) {
        this.base = base;
    }

    // Note: this comparator imposes orderings that are inconsistent with equals.    
    public int compare(String a, String b) {
        if (base.get(a) >= base.get(b)) {
            return -1;
        } else {
            return 1;
        } // returning 0 would merge keys
    }
}