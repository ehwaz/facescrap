package facescrap.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import facescrap.model.Article;
import facescrap.model.TagArticle;

@Component
public class Similarity {
	@Autowired
    private SessionFactory sessionFactory;
	
	public Similarity() {
	}
	
	public Vector<Integer> getSimilarArticles(
			HashMap<String, Integer> extractedNouns, 
			String writerOfPost,
			List<Article> candidates,
			int numOfSample)
	{
		// SortedSet 인터페이스와 커스텀 Pair 클래스 사용. Similarity(=value) 기준으로 정렬되어 들어간다.
		SortedSet<Pair<Integer, Double>> simliarityResult = new TreeSet<Pair<Integer, Double>>();
		
		Iterator<Article> iter = candidates.iterator();
		while (iter.hasNext()) {
			Article someArticle = iter.next();
			
			Criteria criteria = getCurrentSession().createCriteria(TagArticle.class);
			criteria = criteria.add(Restrictions.eq("pk.article", someArticle));
			List<TagArticle> tagsOfSomeArticle = criteria.list();
			
			if (tagsOfSomeArticle.size() > 0) {
				// param으로 쓸 hashmap 구성
				HashMap<String, Integer> param = new HashMap<String, Integer>();
				for (int i=0; i<tagsOfSomeArticle.size(); i++) {
					TagArticle tag = tagsOfSomeArticle.get(i);
					param.put(tag.getTag().getName(), (int)tag.getFrequency());
				}
				
				// 현재 추출된 문자 단어와 similarity 계산
				double similarityValue = this.cosineSimilarity(extractedNouns, param);
				JSONObject articleContent;
				String articleWriterId = null;
				try {
					articleContent = new JSONObject(someArticle.getContent());
					JSONObject temp = (JSONObject) articleContent.get("from");
					articleWriterId = (String) temp.getString("id");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (writerOfPost.equals(articleWriterId)) {
					similarityValue += 0.1;
				}
				
				// Similarity의 최소값을 제한한다.
				if (similarityValue > 0.4) {
					// (현재 candidate의 index, similarityValue)가 입력될때, SortedSet에 의해서 similarityValue의 오름차순으로 정렬된다.
					int index = candidates.indexOf(someArticle);
					simliarityResult.add(new Pair<Integer, Double>(index, similarityValue));
				}
			}
		}
		
		if (simliarityResult.size() == 0) {
			return null;
		}
		
		// numOfSample 이하 개수의 sample article 리턴.
		Vector<Integer> result = new Vector<Integer>();
		Iterator<Pair<Integer, Double>> iterForResult = simliarityResult.iterator();
		int count = 0;
		while (iterForResult.hasNext() && count < numOfSample) {
			Pair<Integer, Double> pair = iterForResult.next();
			result.add(pair.first);
			count++;
		}
		
		return result;
	}
	
	public double cosineSimilarity(
						HashMap<String, Integer> candiatates, 
						HashMap<String, Integer> tagOfSomeArticle) 
    {
		// sameKey.retainAll을 호출하면 sameKey를 key set으로 하는 HashMap의 내용이 날라가므로 copy해서 적용-,.-
		HashMap<String, Integer> argCopy1 = new HashMap<String, Integer>(candiatates);
		HashMap<String, Integer> argCopy2 = new HashMap<String, Integer>(tagOfSomeArticle);
		
		//-- 두 HashMap에서 같은 key인것만 가려내어, 그들의 frequency를 각각 List<Double>에 넣는다. 
		Set<String> sameKey = argCopy1.keySet();
		Set<String> keys2 = argCopy2.keySet();
		sameKey.retainAll(keys2);
		
		if (sameKey.size() == 0) {
			return 0;
		}
		
		String[] sameKeyArr = sameKey.toArray(new String[0]);
		List<Double> docVector1 = new Vector<Double>();
		List<Double> docVector2 = new Vector<Double>();
		
		for (int i=0; i<sameKey.size(); i++) {
			docVector1.add((double)argCopy1.get(sameKeyArr[i]));
			docVector2.add((double)argCopy2.get(sameKeyArr[i]));
		}
		//--
		
        double dotProduct = 0.0;
        double magnitude1 = 0.0;
        double magnitude2 = 0.0;
        double cosineSimilarity = 0.0;

        // 분자 계산. docVector1 and docVector2 must be of same length
        for (int i = 0; i < docVector1.size(); i++) {
            dotProduct += docVector1.get(i) * docVector2.get(i);  //a.b
        }
        
        // 분모 계산.
        for (Integer i : argCopy1.values()) {
        	magnitude1 += Math.pow(i, 2);  //(a^2)
        }
        for (Integer i : argCopy2.values()) {
        	magnitude2 += Math.pow(i, 2);  //(b^2)
        }
        magnitude1 = Math.sqrt(magnitude1);//sqrt(a^2)
        magnitude2 = Math.sqrt(magnitude2);//sqrt(b^2)

        if (magnitude1 != 0.0 | magnitude2 != 0.0)
        {
            cosineSimilarity = dotProduct / (magnitude1 * magnitude2);
        } 
        else 
        {
            return 0.0;
        }
        return cosineSimilarity;
    }
	
	protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
}
