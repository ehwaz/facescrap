package facescrap.service;

import facescrap.model.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * User: Giho
 * Date: 13. 11. 17
 * Time: 오후 4:05
 */
@Repository
@Transactional
public class UserService {

    @Autowired
    private SessionFactory sessionFactory;

    public void add(User user) {
        getCurrentSession().saveOrUpdate(user);
    }

    public User getByEmail(String email) {
        Criteria criteria = getCurrentSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("email", email));
        List results = criteria.list();

        if(results.isEmpty())
            return null;

        return (User) results.get(0);
    }

    public User update(User entity){
        return (User) getCurrentSession().merge(entity);
    }

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
}
