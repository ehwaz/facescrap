package facescrap.service;

import facescrap.model.Tag;
import facescrap.model.TagNode;
import facescrap.model.TagNodeUI;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gihopark
 * Date: 2013. 12. 11.
 * Time: 오후 7:16
 * To change this template use File | Settings | File Templates.
 */
@Repository
@Transactional
public class TagTreeService {

    @Autowired
    private SessionFactory sessionFactory;

    public HashMap<Long, HashSet<Long>> getNodeMap(long userId) {
        HashMap<Long, HashSet<Long>> nodeMap = new HashMap<>();

        List<TagNodeUI> trees = getTrees(userId);
        for (TagNodeUI tree : trees) {
            addChildren(nodeMap, tree);
        }

        return nodeMap;
    }

    private void addChildren(HashMap<Long, HashSet<Long>> nodeMap, TagNodeUI node) {
        HashSet<Long> childrenIds = nodeMap.get(node.getId());

        if (childrenIds == null) {
            childrenIds = new HashSet<>();
            nodeMap.put(node.getId(), childrenIds);
        }

        for (TagNodeUI child : node.getChildren()) {
            childrenIds.add(child.getId());
            addChildren(nodeMap, child);
        }
    }

    public List<TagNodeUI> getTrees(long userId) {
        Criteria criteria = getCurrentSession().createCriteria(TagNode.class);
        criteria.add(Restrictions.eq("userId", userId));

        List<TagNode> nodes = criteria.list();
        List<TagNodeUI> trees = new ArrayList<>();
        HashMap<Long, TagNodeUI> nodeMap = new HashMap<>();

        for (TagNode node : nodes) {
            Tag tag = node.getTag();

            TagNodeUI uiNode = new TagNodeUI();
            uiNode.setId(tag.getTagId());
            uiNode.setName(tag.getName());
            uiNode.setChildren(new ArrayList<TagNodeUI>());

            if (node.getParentTagNodeId() == 0) {
                uiNode.setTagTreeId(node.getTagNodeId());
                trees.add(uiNode);
            } else {
                TagNodeUI parentUINode = nodeMap.get(node.getParentTagNodeId());
                if (parentUINode == null) continue;

                List<TagNodeUI> children = parentUINode.getChildren();
                if (children == null) continue;

                uiNode.setTagTreeId(parentUINode.getTagTreeId());
                children.add(uiNode);
            }

            nodeMap.put(node.getTagNodeId(), uiNode);
        }

        return new ArrayList<>(trees);
    }

    public void updateTree(long userId, TagNodeUI rootNode) {
        long tagTreeId = rootNode.getTagTreeId();
        if (tagTreeId > 0)
            deleteTreeNodes(userId, tagTreeId);
        else
            tagTreeId = addNode(userId, 0, rootNode);

        addChildren(userId, tagTreeId, rootNode);
    }

    public void deleteTree(long userId, long rootNodeId) {
        String hql = "delete from TagNode where parentTagNodeId= :id or tagNodeId= :id";
        getCurrentSession().createQuery(hql).setLong("id", rootNodeId).executeUpdate();
    }

    public void deleteTreeNodes(long userId, long rootNodeId) {
        String hql = "delete from TagNode where parentTagNodeId= :id";
        getCurrentSession().createQuery(hql).setLong("id", rootNodeId).executeUpdate();
    }

    private void addChildren(long userId, long currentNodeId, TagNodeUI uiNode) {
        List<TagNodeUI> children = uiNode.getChildren();
        if (children == null || children.size() == 0)
            return;

        for (TagNodeUI child : children) {
            long childNodeId = addNode(userId, currentNodeId, child);
            addChildren(userId, childNodeId, child);
        }
    }

    private long addNode(long userId, long parentNodeId, TagNodeUI uiNode) {
        Tag tag = new Tag();
        tag.setTagId(uiNode.getId());
        tag.setName(uiNode.getName());

        TagNode node = new TagNode();
        node.setUserId(userId);
        node.setParentTagNodeId(parentNodeId);
        node.setTag(tag);

        getCurrentSession().saveOrUpdate(node);

        return node.getTagNodeId();
    }

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

}
