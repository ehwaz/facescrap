package facescrap.service;

import facescrap.model.Article;
import facescrap.model.Tag;
import facescrap.model.TagArticle;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gihopark
 * Date: 2013. 12. 1.
 * Time: 오후 11:20
 * To change this template use File | Settings | File Templates.
 */
@Repository
@Transactional
public class TagService {
    @Autowired
    private SessionFactory sessionFactory;

    public void tag(long articleId, String tag) {
        if (tag == null) return;
        tag = tag.trim();

        if (tag.length() == 0) return;

        Article article = getArticle(articleId);
        if (article == null) return;

        // TODO: Temp: 유저가 새로/다시 추가하는 단어의 freq를 2로 fix.. Awwwwwwww.....
        TagArticle newTag = new TagArticle(getOrAdd(tag), article, 5);
        article.addTag(newTag);
        getCurrentSession().saveOrUpdate(article);
    }

    public void untag(long articleId, long tagId) {
        Article article = getArticle(articleId);
        if (article == null) return;

        Tag tag = getById(tagId);
        if (tag == null) return;

        TagArticle tagToRemove = new TagArticle(tag, article, 0);
        article.removeTag(tagToRemove);

        getCurrentSession().merge(article);
    }

    public Tag getOrAdd(String name) {
        Tag tag = getByName(name);
        if (tag != null) return tag;

        tag = new Tag();
        tag.setName(name);
        getCurrentSession().saveOrUpdate(tag);

        return tag;
    }

    public List<Tag> getAllTags(long userId) throws IOException {
        HashSet<Tag> tags = new HashSet<>();

        Criteria criteria = getCurrentSession().createCriteria(Article.class);
        criteria.add(Restrictions.eq("userId", userId));
        criteria.addOrder(Order.desc("createdDate"));

        List<Article> articles = criteria.list();
        for (Article article : articles) {
            List<TagArticle> tagsInArticle = article.getTagsInArticle();
            for (TagArticle tagArticle : tagsInArticle) {
                tags.add(tagArticle.getTag());
            }
        }

        return new ArrayList<>(tags);
    }

    private Tag getByName(String name) {
        Criteria criteria = getCurrentSession().createCriteria(Tag.class);
        criteria.add(Restrictions.eq("name", name));
        List results = criteria.list();

        if (results.isEmpty())
            return null;

        return (Tag) results.get(0);
    }

    public Tag getById(long id) {
        Criteria criteria = getCurrentSession().createCriteria(Tag.class);
        criteria.add(Restrictions.eq("tagId", id));
        List results = criteria.list();

        if (results.isEmpty())
            return null;

        return (Tag) results.get(0);
    }

    private Article getArticle(long articleId) {
        Criteria criteria = getCurrentSession().createCriteria(Article.class);
        criteria.add(Restrictions.eq("articleId", articleId));
        List results = criteria.list();

        if (results.isEmpty())
            return null;

        return (Article) results.get(0);

    }

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
}
