package facescrap.service;

import facescrap.facebook.Comment;
import facescrap.facebook.Post;
import facescrap.facebook.Property;
import facescrap.facebook.Reference;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: gihopark
 * Date: 2013. 12. 1.
 * Time: 오후 5:55
 * To change this template use File | Settings | File Templates.
 */
@Service
public class FacebookService {

    private final int PAGE_SIZE = 25, REQUEST_SIZE = 50;
    //TODO Concurrency 지원
    private Dictionary<String, CachedPost> cachedPosts = new Hashtable<>();

    public List<Post> getFeed(String fbToken, int page) throws IOException {
        CachedPost cache = cachedPosts.get(fbToken);
        if (cache == null) {
            cache = new CachedPost();
            cachedPosts.put(fbToken, cache);

            getFromFacebook(fbToken, cache, GetType.None);

            if (page == 0)
                return cache.posts;
        }

        if (page == 0) {
            if ((new Date().getTime() - cache.since.getTime()) > 300) {
                getFromFacebook(fbToken, cache, GetType.Since);
            }

            return cache.posts.subList(0, PAGE_SIZE);
        }

        int from = page * PAGE_SIZE, to = from + PAGE_SIZE;
        if (cache.posts.size() < to) {
            getFromFacebook(fbToken, cache, GetType.Until);
        }

        if (from >= cache.posts.size()) {
            return new ArrayList<>();
        }

        to = Math.min(to, cache.posts.size());
        return cache.posts.subList(from, to);
    }

    public Post getFeed(String fbToken, String id) throws IOException {
        CachedPost cache = cachedPosts.get(fbToken);
        if (cache == null || cache.posts == null)
            return null;

        for (Post post : cache.posts) {
            if (post.getId().equals(id)) {
                return post;
            }
        }

        return null;
    }

    private void getFromFacebook(String fbToken, CachedPost cache, GetType type) throws IOException {
        String baseUrl = "https://graph.facebook.com/me/home?access_token=" + URLEncoder.encode(fbToken) + "&limit=" + REQUEST_SIZE;
        if (type == GetType.Since)
            baseUrl += "&since=" + cache.since.getTime()/1000;
        else if (type == GetType.Until)
            baseUrl += "&until=" + cache.until.getTime()/1000;

        URL url = new URL(baseUrl);
        URLConnection connection = url.openConnection();
        String json = IOUtils.toString(connection.getInputStream());

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.getDeserializationConfig().addMixInAnnotations(Post.class, PostMixin.class);

        JsonNode treeNode = mapper.readTree(json);
        JsonNode dataNode = treeNode.get("data");

        List<Post> posts = new ArrayList<>();
        if (dataNode.isArray()) {
            if (dataNode.size() == 0)
                return;

            for (JsonNode postNode : dataNode) {
                Post post = parsePost(mapper, postNode);
                if (post == null) continue;

                posts.add(post);
            }
        }

        JsonNode pagingNode = treeNode.get("paging");
        String sinceUrl = pagingNode.get("previous").getTextValue().trim();
        String untilUrl = pagingNode.get("next").getTextValue().trim();

        long since = Long.parseLong(getParamValue(sinceUrl, "since"))*1000;
        long until = Long.parseLong(getParamValue(untilUrl, "until"))*1000;

        if (type == GetType.Since) {
            cache.since = new Date(since);
            cache.posts.addAll(0, posts);
        } else if (type == GetType.Until) {
            cache.until = new Date(until);
            cache.posts.addAll(posts);
        } else {
            cache.since = new Date(since);
            cache.until = new Date(until);
            cache.posts = posts;
        }
    }

    private Post parsePost(ObjectMapper mapper, JsonNode postNode) throws IOException {
        Post post = mapper.readValue(postNode, Post.class);

        //TODO 사진 / 비디오?
        if (post.getMessage() == null || post.getMessage().equals(""))
            return null;

        post.setComments(parseList(mapper, postNode.get("comments"), Comment.class));
        post.setTo(parseList(mapper, postNode.get("to"), Reference.class));
        post.setProperties(parseList(mapper, postNode.get("properties"), Property.class));
        post.setLikes(parseList(mapper, postNode.get("likes"), Reference.class));
        return post;
    }

    private String getParamValue(String url, String attr) {
        int from = url.indexOf("&" + attr + "=") + 2 + attr.length();
        int to = url.indexOf("&", from);

        if (to < 0)
            return url.substring(from);
        return url.substring(from, to);
    }

    private <T> List<T> parseList(ObjectMapper mapper, JsonNode listNode, Class<T> itemType) throws IOException {
        if (listNode == null) return null;

        List<T> items = new ArrayList<>();
        JsonNode dataNode = listNode.get("data");
        if (dataNode == null || !dataNode.isArray()) return items;

        for (JsonNode itemNode : dataNode) {
            T item = mapper.readValue(itemNode, itemType);
            items.add(item);
        }

        return items;
    }

    private enum GetType {
        Since, Until, None
    }

    public static class PostMixin {
        @JsonIgnore
        private List<Property> properties;
        @JsonIgnore
        private List<Comment> comments;
        @JsonIgnore
        private List<Reference> to;
        @JsonIgnore
        private List<Reference> likes;
    }

    private class CachedPost {
        public List<Post> posts;
        public Date since;
        public Date until;
    }
}
