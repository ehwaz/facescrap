package facescrap.service;

import facescrap.facebook.Post;
import facescrap.model.Article;
import facescrap.model.ScrapPost;
import facescrap.model.Tag;
import facescrap.model.TagArticle;
import facescrap.util.TagGenerator;

import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: gihopark
 * Date: 2013. 12. 1.
 * Time: 오후 6:36
 * To change this template use File | Settings | File Templates.
 */
@Repository
@Transactional
public class ArticleService {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private TagGenerator tagGenerator;
    @Autowired
    private TagService tagService;
    private ObjectMapper objectMapper = null;

    public ArticleService() {
        objectMapper = new ObjectMapper();
        tagGenerator = new TagGenerator();
    }

    public long scrap(long userId, Post post) throws IOException {
        String jsonPost = objectMapper.writeValueAsString(post);

        Article article = new Article();
        article.setUserId(userId);
        article.setType(post.getType());
        article.setContent(jsonPost);
        article.setCreatedDate(new Date());

        String text = generateText(post);
        
        if (text != null && text.trim().length() > 0) {
            HashMap<String, Integer> recommendedTags = recommendTags(text, post.getFrom().getId());
            
            if (recommendedTags != null) {
	            Iterator<String> iterForRecommand = recommendedTags.keySet().iterator();
	            Vector<TagArticle> tagsInArticle = new Vector<TagArticle>();
	            while (iterForRecommand.hasNext()) {
	            	String keyOfResult = iterForRecommand.next();
	            	Tag tagToAdd = tagService.getOrAdd(keyOfResult);
	            	TagArticle tagInThisArticle = new TagArticle(tagToAdd, article, recommendedTags.get(keyOfResult));
	            	
	            	tagsInArticle.add(tagInThisArticle);
	            }
	            
	            article.setTagsInArticle(tagsInArticle);
            }
        }

        getCurrentSession().saveOrUpdate(article);
        
        return article.getArticleId();
    }

    public HashMap<String, Integer> recommendTags(String text, String writer) {
        // URL 제거
        text = text.replaceAll("\\b((https?|ftp|file)://|)[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]", "");
        // ㅋ, ㅎ, ㅜ, ㅠ, ㅍ, !, , 제거-_-;;
        text = text.replaceAll("[ㅋ|ㅎ|ㅠ|ㅜ|ㅍ|\\!|\\,]*", "");
        // 괄호 공백으로 치환
        text = text.replaceAll("[({})\\[\\]]", " ");

        int numOfResult = 5;	// 추천 결과 단어의 최대 개수
        int numOfSample = 50;	// 단어 추천을 위해 공통되는 단어 tag를 비교할 Article 샘플 개수
        return tagGenerator.recommand(text, writer, numOfResult, numOfSample);
    }

    public void removeAllTags(long articleId) {    	
    	Criteria criteria = getCurrentSession().createCriteria(TagArticle.class);
		criteria = criteria.add(Restrictions.eq("pk.article.articleId", articleId));
		List<TagArticle> tagsOfArticle = criteria.list();
		
		for (TagArticle tagArticle : tagsOfArticle) {
			tagService.untag(articleId, tagArticle.getTag().getTagId());
		}
    }
    
    public void unscrap(long articleId) {    	
        Article article = new Article();
        article.setArticleId(articleId);
        getCurrentSession().delete(article);
    }

    public List<ScrapPost> get(long userId) throws IOException {
        Criteria criteria = getCurrentSession().createCriteria(Article.class);
        criteria.add(Restrictions.eq("userId", userId));
        criteria.addOrder(Order.desc("createdDate"));
        List<Article> articles = criteria.list();

        if (articles.isEmpty())
            return null;

        ArrayList<ScrapPost> posts = new ArrayList<>();

        for (Article article : articles) {
            Post post = objectMapper.readValue(article.getContent(), Post.class);

            ScrapPost scrapPost = new ScrapPost();
            scrapPost.setScrapPostId(article.getArticleId());
            scrapPost.setPost(post);
            scrapPost.setScrappedDate(article.getCreatedDate());
            
            ArrayList<Tag> tags = new ArrayList<>();
            
            for (TagArticle tagArticle : article.getTagsInArticle()) {
            	tags.add(tagArticle.getTag());
            }

            scrapPost.setTags(tags);
            posts.add(scrapPost);
        }

        return posts;
    }
    
    public int getCount(long userId) throws IOException {
    	List<ScrapPost> scraps = get(userId);
    	if (scraps == null)
    		return 0;
    	return scraps.size();
    }

    private String generateText(Post post) {
        String message = post.getMessage();
        String linkContent = post.getDescription();
        if (linkContent != null) {
        	message += " " + linkContent;
        }
        String linkCaption = post.getCaption();
        if (linkCaption != null) {
        	message += " " + linkCaption;
        }
        return message;
    }

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
}
