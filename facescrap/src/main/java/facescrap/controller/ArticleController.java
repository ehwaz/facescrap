package facescrap.controller;

import facescrap.facebook.Post;
import facescrap.facebook.Reference;
import facescrap.model.ScrapPost;
import facescrap.model.Tag;
import facescrap.model.User;
import facescrap.service.ArticleService;
import facescrap.service.FacebookService;
import facescrap.service.TagService;
import facescrap.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.*;

/**
 * User: Giho
 * Date: 13. 11. 17
 * Time: 오후 3:58
 */
@Controller
public class ArticleController extends ControllerBase {

    @Autowired
    private UserService userService;
    @Autowired
    private FacebookService facebookService;
    @Autowired
    private ArticleService articleService;
    @Autowired
    private TagService tagService;

    @RequestMapping("/")
    public String index(final ModelMap model) throws IOException {
        if (getFbToken() == null) {
            return "redirect:fblogin";
        }

        User user = getUser();
        int numScarps = articleService.getCount(user.getUserId());

        model.addAttribute("email", user.getEmail());
        model.addAttribute("imageUrl", "https://graph.facebook.com/me/picture?width=200&height=200&access_token=" + user.getFbToken());
        model.addAttribute("totalScraps", numScarps);

        return "main2";
    }

    @RequestMapping(value = "/scrapbook", method = RequestMethod.GET)
    public String showScraps(final ModelMap model) throws IOException {
        if (getFbToken() == null) {
            return "redirect:fblogin";
        }

        User user = getUser();
        int numScarps = articleService.getCount(user.getUserId());

        model.addAttribute("email", user.getEmail());
        model.addAttribute("imageUrl", "https://graph.facebook.com/me/picture?width=200&height=200&access_token=" + user.getFbToken());
        model.addAttribute("totalScraps", numScarps);

        return "scraps";
    }

    @RequestMapping(value = "/feeds", method = RequestMethod.GET)
    public
    @ResponseBody
    List<Post> getFeed(@RequestParam(defaultValue = "0") int page) throws IOException {
        return facebookService.getFeed(getFbToken(), page);
    }

    @RequestMapping(value = "/scrap", method = RequestMethod.POST)
    public
    @ResponseBody
    long scarp(@RequestParam String id) throws IOException {
        User user = getUser();
        if (user == null) return -1;

        Post post = facebookService.getFeed(user.getFbToken(), id);
        if (post == null) return -1;

        return articleService.scrap(user.getUserId(), post);
    }

    @ResponseBody
    @RequestMapping(value = "/unscrap", method = RequestMethod.POST)
    public void unscrap(@RequestParam long id) {
        articleService.removeAllTags(id);    // M:N 모델 적용에서의 문제 해결을 위해 부린 꼼수-,.-..
        articleService.unscrap(id);
    }

    @RequestMapping(value = "/scraps", method = RequestMethod.GET)
    public
    @ResponseBody
    List<ScrapPost> getScraps(@RequestParam(value = "sc", defaultValue = "NONE") SortColumn sortColumn, @RequestParam(required = false) String tags) throws IOException {
        User user = getUser();
        List<ScrapPost> posts = articleService.get(user.getUserId());

        List<Long> tagIds = new ArrayList<>();
        if (tags != null) {
            for (String tag : tags.split(",")) {
                try {
                    tagIds.add(Long.parseLong(tag));
                } catch (Exception e) {
                    continue;
                }
            }
        }

        if (tagIds.size() > 0) {
            HashSet<Tag> tagsToCheck = new HashSet<>();
            for (long tagId : tagIds) {
                Tag tag = new Tag();
                tag.setTagId(tagId);

                tagsToCheck.add(tag);
            }

            List<ScrapPost> postsWithTag = new ArrayList<>();
            for (ScrapPost post : posts) {
                for (Tag tag : post.getTags()) {
                    if (tagsToCheck.contains(tag)) {
                        postsWithTag.add(post);
                        break;
                    }
                }
            }

            posts = postsWithTag;
        }

        switch (sortColumn) {
            case LIKES:
                Collections.sort(posts, new Comparator<ScrapPost>() {
                    @Override
                    public int compare(ScrapPost o1, ScrapPost o2) {
                        Post post1 = o1.getPost(), post2 = o2.getPost();
                        Integer numLikes1 = 0, numLikes2 = 0;

                        if (post1 != null && post1.getLikes() != null)
                            numLikes1 = post1.getLikes().size();
                        if (post2 != null && post2.getLikes() != null)
                            numLikes2 = post2.getLikes().size();

                        return -1 * numLikes1.compareTo(numLikes2);
                    }
                });
                break;

            case COMMENTS:
                Collections.sort(posts, new Comparator<ScrapPost>() {
                    @Override
                    public int compare(ScrapPost o1, ScrapPost o2) {
                        Post post1 = o1.getPost(), post2 = o2.getPost();
                        Integer numComments1 = 0, numComments2 = 0;

                        if (post1 != null && post1.getComments() != null)
                            numComments1 = post1.getComments().size();
                        if (post2 != null && post2.getComments() != null)
                            numComments2 = post2.getComments().size();

                        return -1 * numComments1.compareTo(numComments2);
                    }
                });
                break;

            case POSTER:
                Collections.sort(posts, new Comparator<ScrapPost>() {
                    @Override
                    public int compare(ScrapPost o1, ScrapPost o2) {
                        return getName(o1).compareTo(getName(o2));
                    }

                    private String getName(ScrapPost scrap) {
                        if (scrap == null) return "";

                        Post post = scrap.getPost();
                        if (post == null) return "";

                        Reference from = post.getFrom();
                        if (from == null) return "";

                        String name = from.getName();
                        if (name == null) return "";

                        return name;
                    }
                });
                break;
        }

        return posts;
    }

    private User getUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }

        return userService.getByEmail(username);
    }

    private String getFbToken() {
        User user = getUser();
        if (user == null || user.getFbToken() == null) {
            return null;
        }

        return user.getFbToken();
    }

    public enum SortColumn {
        NONE, LIKES, POSTER, COMMENTS
    }
}
