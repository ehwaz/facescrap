package facescrap.controller;

import facescrap.model.User;
import facescrap.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.social.connect.Connection;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.GrantType;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * User: Giho
 * Date: 13. 11. 24
 * Time: 오후 4:28
 */
@Controller
public class FacebookController {

    @Autowired
    private UserService userService;

    private final String appId = "600322130029568", appSecret = "357291e5085a74eb644923c7421e523d", redirectUrl = "http://localhost:8080/fbconfirm";

    @RequestMapping(value = "/fblogin")
    public String redirectLogin() {
        FacebookConnectionFactory connectionFactory = new FacebookConnectionFactory(appId, appSecret);
        OAuth2Operations oauthOperations = connectionFactory.getOAuthOperations();
        OAuth2Parameters params = new OAuth2Parameters();
        params.setScope("email,read_stream,user_photos,user_videos,friends_photos,friends_videos,user_status,friends_status,user_events,friends_events,user_groups,friends_groups,user_likes,friends_likes");
        params.setRedirectUri(redirectUrl);

        String authorizeUrl = oauthOperations.buildAuthorizeUrl(GrantType.AUTHORIZATION_CODE, params);
        return "redirect:" + authorizeUrl;
    }

    @RequestMapping(value = "/fbconfirm")
    public String confirmLogin(@RequestParam String code) {
        FacebookConnectionFactory connectionFactory = new FacebookConnectionFactory(appId, appSecret);
        OAuth2Operations oauthOperations = connectionFactory.getOAuthOperations();

        AccessGrant accessGrant = oauthOperations.exchangeForAccess(code, redirectUrl, null);

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }

        User user = userService.getByEmail(username);
        user.setFbToken(accessGrant.getAccessToken());
        userService.update(user);

        return "redirect:/";
    }

    @ResponseBody
    @RequestMapping(value = "/fbtoken", method = RequestMethod.GET)
    public String getAccessToken(){
        return getFbToken();
    }

    private User getUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }

        return userService.getByEmail(username);
    }

    private String getFbToken() {
        User user = getUser();
        if (user == null || user.getFbToken() == null) {
            return null;
        }

        return user.getFbToken();
    }
}
