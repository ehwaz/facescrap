package facescrap.controller;

import facescrap.model.Login;
import facescrap.model.Registration;
import facescrap.model.User;
import facescrap.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * User: Giho
 * Date: 13. 11. 17
 * Time: 오후 2:16
 */
@Controller
public class UserController extends ControllerBase  {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLogin(final Login login) {
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String doLogin(final Login login, final BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "login";
        }

        User user = userService.getByEmail(login.getEmail());
        if(user == null || !user.getPassword().equals(login.getPassword())){
            return "login";
        }

        return "redirect:/";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String showSignup(final Registration registration) {
        return "signup";
    }

    @Transactional
    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String doSignup(final Registration registration, final BindingResult bindingResult, final ModelMap model) {
        if (bindingResult.hasErrors()) {
            return "signup";
        }

        User user = new User();
        user.setEmail(registration.getEmail());
        user.setPassword(registration.getPassword());

        userService.add(user);

        return "redirect:/login";
    }
}
