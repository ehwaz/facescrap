package facescrap.controller;

import facescrap.model.Tag;
import facescrap.model.TagNodeUI;
import facescrap.model.User;
import facescrap.service.ArticleService;
import facescrap.service.TagService;
import facescrap.service.TagTreeService;
import facescrap.service.UserService;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;

import java.io.IOException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: gihopark
 * Date: 2013. 12. 1.
 * Time: 오후 11:38
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class TagController {
    @Autowired
    private TagService tagService;
    @Autowired
    private UserService userService;
    @Autowired
    private ArticleService articleService;
    @Autowired
    private TagTreeService tagTreeService;

    @RequestMapping("/searchtags")
    public String search(@RequestParam String keyword, final ModelMap model) throws IOException {
        HashSet<Long> tagIds = new HashSet<>();

        List<Tag> allTags = tagService.getAllTags(getUser().getUserId());
        for (Tag tag : allTags) {
            if (tag.getName().toUpperCase().contains(keyword.toUpperCase())) {
                tagIds.add(tag.getTagId());
            }
        }

        HashMap<Long, HashSet<Long>> nodeMap = tagTreeService.getNodeMap(getUser().getUserId());
        Queue<Long> tagIdsQueue = new LinkedList<>(tagIds);

        while (!tagIdsQueue.isEmpty()) {
            long tagId = tagIdsQueue.remove();

            HashSet<Long> childrenIds = nodeMap.get(tagId);
            if (childrenIds == null || childrenIds.size() == 0)
                continue;

            for (long childId : childrenIds) {
                if (tagIds.add(childId)) {
                    tagIdsQueue.add(childId);
                }
            }
        }


        if (tagIds.size() > 0) {
            return "redirect:scrapbook?tags=" + StringUtils.join(tagIds, ",");
        }

        User user = getUser();
        int numScarps = articleService.getCount(user.getUserId());

        model.addAttribute("email", user.getEmail());
        model.addAttribute("imageUrl", "https://graph.facebook.com/me/picture?width=200&height=200&access_token=" + user.getFbToken());
        model.addAttribute("totalScraps", numScarps);

        return "empty";
    }

    @RequestMapping("/managetags")
    public String manageTags(final ModelMap model) throws IOException {

        User user = getUser();
        int numScarps = articleService.getCount(user.getUserId());

        model.addAttribute("email", user.getEmail());
        model.addAttribute("imageUrl", "https://graph.facebook.com/me/picture?width=200&height=200&access_token=" + user.getFbToken());
        model.addAttribute("totalScraps", numScarps);

        return "tag_manage";
    }

    @ResponseBody
    @RequestMapping(value = "/tag/{id}", method = RequestMethod.POST)
    public void tag(@PathVariable long id, @RequestParam String name) {
        tagService.tag(id, name);
    }

    @ResponseBody
    @RequestMapping(value = "/untag/{id}", method = RequestMethod.POST)
    public void untag(@PathVariable long id, @RequestParam long tagId) {
        tagService.untag(id, tagId);
    }

    @RequestMapping(value = "/tags", method = RequestMethod.GET)
    public
    @ResponseBody
    List<Tag> get() throws IOException {
        return tagService.getAllTags(getUser().getUserId());
    }

    @RequestMapping(value = "/trees/delete", method = RequestMethod.POST)
    @ResponseBody
    public void deleteTree(@RequestParam long id) {
        tagTreeService.deleteTree(getUser().getUserId(), id);
    }

    @ResponseBody
    @RequestMapping(value = "/tree", method = RequestMethod.POST)
    public void updateTree(@RequestParam String tree) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        TagNodeUI tagNodeUI = mapper.readValue(tree, TagNodeUI.class);
        tagTreeService.updateTree(getUser().getUserId(), tagNodeUI);
    }

    @RequestMapping(value = "/trees", method = RequestMethod.GET)
    public
    @ResponseBody
    List<TagNodeUI> getTrees() {
        return tagTreeService.getTrees(getUser().getUserId());
    }

    private User getUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }

        return userService.getByEmail(username);
    }
}
