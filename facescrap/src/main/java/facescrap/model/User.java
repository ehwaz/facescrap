package facescrap.model;

import javax.persistence.*;

/**
 * User: Giho
 * Date: 13. 11. 17
 * Time: 오후 4:06
 */
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long userId;
    @Column(nullable = false, unique = true)
    private String email = null;
    @Column(nullable = false)
    private String password = null;
    @Column(length = 1024)
    private String fbToken = null;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFbToken() {
        return fbToken;
    }

    public void setFbToken(String fbToken) {
        this.fbToken = fbToken;
    }

    // @ManyToOne article?
}
