package facescrap.model;


import facescrap.facebook.Post;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: gihopark
 * Date: 2013. 12. 2.
 * Time: 오후 12:39
 * To change this template use File | Settings | File Templates.
 */
public class ScrapPost {
    private long scrapPostId;
    private Post post;
    private Date scrappedDate;
    private List<Tag> tags;

    public ScrapPost() {
    }

    public long getScrapPostId() {
        return scrapPostId;
    }

    public void setScrapPostId(long scrapPostId) {
        this.scrapPostId = scrapPostId;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Date getScrappedDate() {
        return scrappedDate;
    }

    public void setScrappedDate(Date scrappedDate) {
        this.scrappedDate = scrappedDate;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

}
