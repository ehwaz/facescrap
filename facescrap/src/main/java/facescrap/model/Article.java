package facescrap.model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gihopark
 * Date: 2013. 12. 1.
 * Time: 오후 6:37
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long articleId;
    @Column
    private long userId;
    @Column
    private String type;
    @Column(columnDefinition = "LONGTEXT")
    private String content;
    @Column
    private Date createdDate;
    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "pk.article",
            cascade = {CascadeType.ALL})
    @Cascade({
//	   org.hibernate.annotations.CascadeType.SAVE_UPDATE,
            org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    private List<TagArticle> tagsInArticle;

    public Article() {
        tagsInArticle = new ArrayList<TagArticle>();
    }

    public long getArticleId() {
        return articleId;
    }

    public void setArticleId(long articleId) {
        this.articleId = articleId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public List<TagArticle> getTagsInArticle() {
        return this.tagsInArticle;
    }

    public void setTagsInArticle(List<TagArticle> input) {
        this.tagsInArticle = input;
    }

    public void removeTag(TagArticle tag) {
        this.tagsInArticle.remove(tag);
    }

    public void addTag(TagArticle tag) {
        this.tagsInArticle.add(tag);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Article that = (Article) o;

        if (this.articleId != that.articleId)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (articleId ^ (articleId >>> 32));
    }
}