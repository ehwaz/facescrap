package facescrap.model;

import java.io.Serializable;

import javax.persistence.*;

@Embeddable
public class TagArticlePk implements Serializable {

	@ManyToOne
    private Article article;
	
	@ManyToOne
    private Tag tag;
	
	public TagArticlePk() {		
	}
	
	// getArticle, setArticle
	public Article getArticle() {
		return article;
	}
	  
	public void setArticle(Article article) {
		this.article = article;
	}

	// getTag, setTag
	public Tag getTag() {
		return tag;
	}
	
	public void setTag(Tag tag) {
		this.tag = tag;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this== o) return true;
		if (o ==null|| getClass() != o.getClass()) return false;
			  
		TagArticlePk that = (TagArticlePk) o;
		
		if (article == null || tag == null 
				|| that.getArticle() == null 
				|| that.getTag() == null)
			return false;

		if ( (article.getArticleId() != that.article.getArticleId()) 
			|| (tag.getTagId() != that.tag.getTagId()) )
				return false;
			  
		return true;
	}
	  
	@Override
	public int hashCode() {
		int result;
		result = (article !=null? article.hashCode() : 0);
		result =31* result + (tag !=null? tag.hashCode() : 0);
		return result;
	}
}
