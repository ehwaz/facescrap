package facescrap.model;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: gihopark
 * Date: 2013. 12. 1.
 * Time: 오후 11:20
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long tagId;

    @Column(unique = true)
    private String name;
    
//    @OneToMany(fetch = FetchType.LAZY, mappedBy ="pk.tag")
//    private List<TagArticle> articlesHasTag = new ArrayList<TagArticle>();

    public long getTagId() {
        return tagId;
    }

    public void setTagId(long tagId) {
        this.tagId = tagId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
//    public List<TagArticle> getArticlesHasTag() {
//        return this.articlesHasTag;
//	}
//	
//	public void setArticlesHasTag(List<TagArticle> input) {
//	        this.articlesHasTag = input;
//	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tag tag = (Tag) o;

        if (tagId != tag.tagId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (tagId ^ (tagId >>> 32));
    }
}
