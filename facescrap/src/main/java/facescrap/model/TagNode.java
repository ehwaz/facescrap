package facescrap.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gihopark
 * Date: 2013. 12. 11.
 * Time: 오후 3:52
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class TagNode {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long tagNodeId;

    @Column
    private long userId;

    @Column
    private long parentTagNodeId;

    @ManyToOne
    private Tag tag;

    public long getTagNodeId() {
        return tagNodeId;
    }

    public void setTagNodeId(long tagNodeId) {
        this.tagNodeId = tagNodeId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getParentTagNodeId() {
        return parentTagNodeId;
    }

    public void setParentTagNodeId(long parentTagNodeId) {
        this.parentTagNodeId = parentTagNodeId;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
}
