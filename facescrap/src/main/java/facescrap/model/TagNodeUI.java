package facescrap.model;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gihopark
 * Date: 2013. 12. 11.
 * Time: 오후 7:18
 * To change this template use File | Settings | File Templates.
 */
public class TagNodeUI {
    private long tagTreeId;
    private long id;
    private String name;
    private List<TagNodeUI> children;

    public long getTagTreeId() {
        return tagTreeId;
    }

    public void setTagTreeId(long tagTreeId) {
        this.tagTreeId = tagTreeId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TagNodeUI> getChildren() {
        return children;
    }

    public void setChildren(List<TagNodeUI> children) {
        this.children = children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TagNodeUI tagNodeUI = (TagNodeUI) o;

        if (id != tagNodeUI.id) return false;
        if (tagTreeId != tagNodeUI.tagTreeId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (tagTreeId ^ (tagTreeId >>> 32));
        result = 31 * result + (int) (id ^ (id >>> 32));
        return result;
    }
}
