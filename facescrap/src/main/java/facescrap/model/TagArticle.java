package facescrap.model;

import javax.persistence.*;

@Entity
@AssociationOverrides( {
			@AssociationOverride(name ="pk.article", joinColumns = @JoinColumn(name ="articleId")),
			@AssociationOverride(name ="pk.tag", joinColumns = @JoinColumn(name ="tagId"))
		})
public class TagArticle {
	@EmbeddedId
	private TagArticlePk pk = new TagArticlePk();
	
	@Column
	private long frequency;
	
	// getPk, setPk
	private TagArticlePk getPk() {
		return pk;
	}
	
	private void setPk(TagArticlePk pk) {
		this.pk = pk;
	}
	
	public TagArticle() {
		this.frequency = 0;
	}
	
	public TagArticle(Tag newTag, Article currentArticle, long frequency) {
		this.setTag(newTag);
		this.setArticle(currentArticle);
		this.frequency = frequency;
	}
	
	// getArticle, setArticle
	public Article getArticle() {
		return getPk().getArticle();
	}
	
	public void setArticle(Article article) {
		getPk().setArticle(article);
	}
	
	// getTag, getTag
	public Tag getTag() {
		return getPk().getTag();
	}
	  
	public void setTag(Tag tag) {
		getPk().setTag(tag);
	}
	
	// getFrequency, setFrequency
	public long getFrequency() {
		return frequency;
	}
	  
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this== o) return true;
		if (o ==null|| getClass() != o.getClass()) return false;
		  
		TagArticle that = (TagArticle) o;
		
		if (pk == null || that.getPk() == null)
			return false;

		if (pk.equals(that.getPk()) == false)
			return false;
	
		return true;
	}
	  
	@Override
	public int hashCode() {
		return (getPk() !=null? getPk().hashCode() : 0);
	}
}
