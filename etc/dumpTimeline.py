# facepy 패키지 필요. pip install facepy로 설치.
# python 2.7 기반.

import sys, json
from facepy import GraphAPI

oauth_token = "CAACEdEose0cBAFCr3ap8HnPIXXrgYE31ZAUgAZCN6dRNvAHZC7cXDl8yKyWLEuMdmqYK6jRQXKhLyvysQkxghjMrnX2J6b7qFbB0X4wTahuGSw4mTdWCowU57aEj0fLIBZACmp70TnFRVK4CHe3zZCTZAbxnLoT0arHCW027xAN7G5OuGWdYvF7FED6ZC6FG9CfxtEFbizp7gZDZD"

graph = GraphAPI(oauth_token)
outfile = open('timeline.json', 'a')
outfile.write("[\n")

#data = graph.get('me/home?limit=10')
count = 0
#request = 'me/feed?limit=1000?fields=message,from'
request = 'me/home?limit=1000?fields=message,from'
while count < 1000:
    data = graph.get(request)
    postList = data["data"]
    for post in postList:
        dictToWrite = {}
        if 'message' in post:
            message = post["message"]
            if len(message) > 0:
		count += 1
    	        id = post["id"]
	        writer_name = post["from"]["name"]
	        writer_id = post["from"]["id"]
	        utf8_message = message.encode("utf-8")
	        dictToWrite["id"] = id
	        dictToWrite["writer_name"] = writer_name.encode("utf-8")
	        dictToWrite["writer_id"] = writer_id
	        dictToWrite["message"] = message.encode("utf-8")
	        # ensure_ascii가 기본값인 true일 경우, 한글 텍스트를 json 파일에 저장할때 UTF-8 인코딩 문자열이 아니라 유니코드 문자열을 넣는다. 
	        # (위처럼 UTF-8 문자열을 dictionary에 넣어도 유니코드로 자체 변환-_-을 한다!)
	        json.dump(dictToWrite, outfile, ensure_ascii=False)
	        outfile.write(',\n')
	# Graph API로부터 여러 post를 'page'단위로 나누어 받기 위함.
    if 'paging' in data:
        request = data["paging"]["next"]
	request = request[26:]
	print request
    else:
		print 'no next page info!'
	break

print 'count: ' + str(count)

outfile.write("]")
outfile.close()
