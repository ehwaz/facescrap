package com.fbexercise.core.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import com.fbexercise.core.domain.FBUser;


@Controller
public class SampleController {
	
//	@Autowired
//	private static FBApiFetcher fbApiFetcher;
	
	private FBUser fetchPersonInfo(String userid) {
		RestTemplate restTemplate = new RestTemplate();
		FBUser fbUser = restTemplate.getForObject("http://graph.facebook.com/" + userid, FBUser.class);
		return fbUser;
	}
	
	@RequestMapping("home")
	public String loadHomePage(Model m) {
		m.addAttribute("name", "Facebook");
		return "home";
	}
	
	@RequestMapping("user")
	public String userView(Model m) {
		FBUser user = fetchPersonInfo("haewon.jeong.66");
		m.addAttribute("first_name", user.getFirst_name());
		m.addAttribute("last_name", user.getLast_name());
		m.addAttribute("link", user.getLink());
		return "user";
	}
	
	
}
