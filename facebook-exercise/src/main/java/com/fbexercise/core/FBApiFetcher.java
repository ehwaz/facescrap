package com.fbexercise.core;

import org.springframework.web.client.RestTemplate;

import com.fbexercise.core.domain.FBUser;

public class FBApiFetcher {
	
	public FBUser fetchPersonInfo(String userid) {
		RestTemplate restTemplate = new RestTemplate();
		FBUser fbUser = restTemplate.getForObject("http://graph.facebook.com/" + userid, FBUser.class);
		return fbUser;
	}
}
