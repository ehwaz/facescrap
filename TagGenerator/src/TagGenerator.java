import java.util.LinkedList;
import java.util.Scanner;

import kr.ac.kaist.swrc.jhannanum.comm.Eojeol;
import kr.ac.kaist.swrc.jhannanum.comm.Sentence;
import kr.ac.kaist.swrc.jhannanum.hannanum.Workflow;
//import kr.ac.kaist.swrc.jhannanum.hannanum.WorkflowFactory;
import kr.ac.kaist.swrc.jhannanum.plugin.MajorPlugin.MorphAnalyzer.ChartMorphAnalyzer.ChartMorphAnalyzer;
import kr.ac.kaist.swrc.jhannanum.plugin.MajorPlugin.PosTagger.HmmPosTagger.HMMTagger;
import kr.ac.kaist.swrc.jhannanum.plugin.SupplementPlugin.MorphemeProcessor.UnknownMorphProcessor.UnknownProcessor;
import kr.ac.kaist.swrc.jhannanum.plugin.SupplementPlugin.PlainTextProcessor.InformalSentenceFilter.InformalSentenceFilter;
import kr.ac.kaist.swrc.jhannanum.plugin.SupplementPlugin.PlainTextProcessor.SentenceSegmentor.SentenceSegmentor;
import kr.ac.kaist.swrc.jhannanum.plugin.SupplementPlugin.PosProcessor.NounExtractor.NounExtractor;

public class TagGenerator {

	public static void main(String[] args) {
		// Predefined Workflow from JAR file.
//		Workflow workflow = WorkflowFactory.getPredefinedWorkflow(WorkflowFactory.WORKFLOW_NOUN_EXTRACTOR);
		
		// Create workflow. (Same Workflow as above one)
		Workflow workflow = new Workflow();
		workflow.appendPlainTextProcessor(new SentenceSegmentor(), null);
		workflow.appendPlainTextProcessor(new InformalSentenceFilter(), null);
		
		workflow.setMorphAnalyzer(new ChartMorphAnalyzer(), "conf/plugin/MajorPlugin/MorphAnalyzer/ChartMorphAnalyzer.json");
		workflow.appendMorphemeProcessor(new UnknownProcessor(), null);
		
		workflow.setPosTagger(new HMMTagger(), "conf/plugin/MajorPlugin/PosTagger/HmmPosTagger.json");
		workflow.appendPosProcessor(new NounExtractor(), null);
		
		Scanner keyboard = new Scanner(System.in);
		
		try {
			/* Activate the work flow in the thread mode */
			/* Once a work flow is activated, it can be used repeatedly. */
			workflow.activateWorkflow(true);
			
			String document = "dummy value";
			int turn = 1;
			while (true) {
				System.out.println("============ " + String.valueOf(turn) + "번째 턴 시작 ============");
				System.out.println("아무 문장이나 넣고 싶은거 함 넣어보드라고!!");
				document = keyboard.nextLine();
				if (document.length() == 0) {
					System.out.println("============ " + String.valueOf(turn) + "번째 턴: 프로그램 끝! ============");
					break;
				}
				/* Analysis using the work flow */
				workflow.analyze(document);
				
				LinkedList<Sentence> resultList = workflow.getResultOfDocument(new Sentence(0, 0, false));
				
				//---- 아예 필요없을 것 같은 명사들 골라내기. 
				// TODO: 나중에 이 filtering을 포함하여 NounExtractor를 custom plug-in으로 수정해서 써도 될듯.
				for (Sentence s : resultList) {
					Eojeol[] eojeolArray = s.getEojeols();
					for (int i = 0; i < eojeolArray.length; i++) {
						if (eojeolArray[i].length > 0) {
							String[] tags = eojeolArray[i].getTags();
							for (int j = 0; j < eojeolArray[i].length; j++) {
								String tag = tags[j];
								/*
								 * 명사 종류에 대해서는 한나눔 메뉴얼 17페이지를 참조할 것.
								 * 아예 필요없을 것 같은 명사:
								 * 		지시대명사(npd): 무엇, 아무것...
								 * 		비단위성 의존 명사(nbs, nbn): 즈음, 짓...
								 * 		단위성 의존 명사(nbu): 다발, 다...
								 */
								if ( (tag.equals("npd")) || (tag.equals("nbs")) || (tag.equals("nbn")) || (tag.equals("nbu")) ) {
									eojeolArray[i].setMorpheme(j, "");
									eojeolArray[i].setTag(j, "");
								}
							}
						}
					}
				}
				//----
				
				for (Sentence s : resultList) {
					Eojeol[] eojeolArray = s.getEojeols();
					for (int i = 0; i < eojeolArray.length; i++) {
						if (eojeolArray[i].length > 0) {
							String[] morphemes = eojeolArray[i].getMorphemes();
							for (int j = 0; j < morphemes.length; j++) {
								System.out.print(morphemes[j]);
							}
							System.out.print(", ");
						}
					}
					System.out.println();
					for (int i = 0; i < eojeolArray.length; i++) {
						if (eojeolArray[i].length > 0) {
							String[] tags = eojeolArray[i].getTags();
							for (int j = 0; j < tags.length; j++) {
								System.out.print(tags[j]);
							}
							System.out.print(", ");
						}
					}
					System.out.println();
				}
				
				System.out.println("============ " + String.valueOf(turn) + "번째 턴 완료 ============");
				turn++;
			}
			
			workflow.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		/* Shutdown the work flow */
		workflow.close();  	
	}

}
